///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file aku.cpp
/// @version 1.0
///
/// Exports data about all aku fish
///
/// @author eduardo kho jr <eduardok@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   2/18/2021
///////////////////////////////////////////////////////////////////////////////

#include <string>
#include <iostream>

#include "aku.hpp"

using namespace std;

namespace animalfarm {

Aku::Aku( int weight, enum Color newColor, enum Gender newGender ) {
	gender = newGender;
	species = "Katsuwonus pelamis";
	scaleColor = newColor;
	favoriteTemp = 75;
	weight = 15;
}

const string Aku::speak(){
	return string( "Bubble Bubble" );
}

void Aku::printInfo(){
	cout << "Aku" << endl;
	cout << "   weight = [" << 15 << "]" << endl;
	Fish::printInfo();


}

} //namespace animalfarm


